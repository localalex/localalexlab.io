---
title: About
subtitle: This blog is an exploration of what a school can look like when innovations in education are intentionally practiced.
comments: false
---

My aim is to ask questions that can lead to discussions and down the line, hopefully, to better solutions. I am, like everyone else, biased. To better understand my perspective, see my [professional bio](../bio) which reflects the path I traversed before ending up in the world of K-12 education.
