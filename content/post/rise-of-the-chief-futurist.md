---
title: Rise of the Chief Futurist
subtitle: Embracing the Chief Futurist visionary role in an educational institution
date: 2021-06-22
comments: false
---
 
***We will do a much better job of preparing our students if schools fully embrace at the organizational level the value of future-forward thinking.***
 
The recent Stanford d.school’s discussion [Educators as Futurists: A Conversation with Stanford d.school Leaders](https://education-reimagined.org/educators-as-futurists-a-conversation-with-stanford-d-school-leaders/) provided insight into the fundamental role of schooling.
 
> Educators who take a futurist’s mindset are looking to cultivate young people into lifelong, adaptive learners, rather than being concerned with teaching what’s immediately measured on a test. From the perspective of an administrator or district leader, it’s about getting comfortable asking bigger questions, such as “What is the role of schools?” and "How might teachers foster and model agency, flexibility, and resiliency?"
>
> -- <cite>Lisa Kay Solomon, Designer In Residence, Stanford D.School</cite>
 
I couldn't agree more. The mission of Education must be not only to keep up with the current world outside as it is functioning today, but to educate our students with an eye to the world of the future. We must aim to be ahead of the present.
 
It’s fair to say that running a school is a great challenge for which many in the business world do not have proper appreciation. Schools are delicate operations that can’t necessarily afford to take big risks. There is so much at stake in educating our young. Changes in the classroom have a direct effect on students: if the introduction of new elements are not well-thought-out, we might disrupt students’ learning routines. The last two years of pandemic serve as an example of just how vulnerable schools are. 
 
Though educators and teachers did adapt to remote learning and hybrid classrooms as best they could, how many of us really recognize the fundamental lesson of adaptability and practical remote work experience that covid presented? In particular, most schools failed to use this opportunity to turn this experience into new practical lesson plans on how the world of fully remote teams and enterprises work. Take GitLab as a real-life model, with their [remote playbook](https://about.gitlab.com/company/culture/all-remote/). This is just the way that many of our students will be working when they become professionals. Remote schooling is a truly unique learning opportunity. We could do more to explore its educational potential.
 
This brings me to the obvious question of ‘Why’. Schools exist in the bubble of their own universe, where the education system largely operates on the same time-tested principles. All efforts are directed at maintaining the existing school processes. With such constraints, innovation is inevitably [kept on a short leash](https://www.collinsdictionary.com/us/dictionary/english/keep-someone-on-a-short-leash). 
 
Is this what the role of a school is?
 
Fundamentally, there are two key objectives that schools are pursuing. First, we are giving students knowledge and testing them to make sure they learn it (or at worst, memorize it). This default knowledge base, defined and imposed by either state or independent standards, qualifies our students to enter college or the workplace.
 
Secondly, we are helping students build their agency, character, creativity, resilience and other life skills so that they can succeed in their professional lives. This is where I see the problem. Yes, we are trying our best - we are not ignoring it - but maybe we lack the ability and vision to succeed at this task.
 
We have fallen out of sync with the real world. This is because schools are largely not aware of just how much today’s business world has evolved, nor the future challenges that their students will face.
 
This is where the Chief Futurist position comes in. School or district leadership can step in to establish a formal role that will lead a forward thinking agenda within an educational institution.
 
How do we do this?
 
The typical academic dean is usually an accomplished veteran teacher who provides guidance in determining best practices in teaching. De facto, individuals in these positions have limited direct experience with how the modern business world works. Given the job prerequisites, they are insiders within the school cultural bubble and often overlook and under-appreciate the realities of the business world and its future-bound prospects.
 
Schools are very hierarchical. Everywhere else, this type of top-down management culture is quickly becoming outdated. For this reason, schools need to learn that positions should be fitted to people’s specific talents, not the other way around. Flexibility and being opportunistic is the key. For instance, when deciding on the Chief Futurist role, if the instructional leader is appropriately skilled in bringing future-thinking influence to the school’s educational vision, he/she can take on Chief Futurist responsibilities. On the other hand, it might be more practical to establish a separate Chief Futurist position and hire someone who has gained skills and experiences outside of the world of education. Yet another possibility is to combine the Technology leadership position with the Chief Futurist role. Often experienced technology professionals have the talent, professional background and personal capacity for supporting the future-forward vision. Basically, I am noting that the Chief Futurist role is as much about the individual’s personality traits as his/her professional background.
 
***Hiring a Chief Futurist or assigning someone those responsibilities is not enough.***
 
School management and its board need to be united behind this vision. The school culture needs to be appropriately shaped. This can be compared with well-established businesses who are faced with the prospect of falling behind where they are forced to implement organization-wide changes. Schools can evolve in the same way and it can only be done through leadership.
 
In summary, due to the inherent nature of schools as protectors of their underage pupils, they also act as gatekeepers of existing educational norms. Given our rapidly evolving world, this paradigm is now at odds with the future preparedness needs of our students. Systemic change and organizational growth are so slow that it is sometimes seemingly impossible.
 
Innovation in schools can come from many angles. It can be parent-, student- or teacher-driven. But first and foremost, school leadership must be fully committed to continuously adapting the students’ experience toward the most desired “future-proof” learning.
 
The two key learning objectives I mentioned above - the knowledge base and student preparedness for the future - are equally important. Schools can’t just claim, “we are traditionally strong academically, so that is our only focus”. These two elements cannot be divorced. Academic learning is only part of the school education. The focus must always be to ensure our students can thrive in the future by learning a mindset and the accompanying skills that correlate with the future reality.


