---
title: Once upon a time, in a school far, far away
subtitle: A fairy tale for school administrators
date: 2022-06-07
comments: false
---
 
I stumbled today on this description, posted somewhere in another city on a school website:

> “The mission of the Technology Department is to ensure that staff and administrators have continual opportunities to improve their professional skills and integration of technology to enhance instructional practices, communication, and productivity”.

Sounds about right? If you read this mission statement carefully, you’ll notice the entire statement is about supporting the technological experiences of teachers and staff. It does not consider the experiences of the students who are on the receiving end of our technology tools. As logic goes, if we take care of the teachers, the students will be handled as well, end of story. 

Yikes! Let me tell you a different story.

Once upon a time, in a school far, far away (actually, around the corner from where you live now), there was a school that had no technology. And then, sometime later, it did have it! The school folks were all overjoyed and impressed with this, but then they discovered their technology often did not do what they wanted it to do. It was confusing and often breaking and malfunctioning. This went on for a year and then everyone had a terrible headache from all of this technological kerfuffle. Oh no!, everyone thought, and they hired a technology wizard, someone who would keep these troubles at bay … at least somewhat, a bit here, a little there.

Time passed by, and now this same school found itself in the midst of a technological terrarium, full of apps, systems and ecosystems. There were now Informational and Educational kinds of technologies and there were now different wizards to handle each - sometimes even two or more people per one kind. And so of course, to manage all of these wizards, they brought a Grand Wizard, a technology director. This special being would be in charge of not only handling current IT and ET headaches, she or he would also use special wizarding powers to look into the future and prepare the school for the onslaught of new technologies (along with all the new challenges and headaches this would inevitably bring).

In other words, someone who could write a Technology Strategic plan. The core part of this strategic plan would be a mission statement such as the one with which I started this post. It is something quite typical, something that almost all schools have. And so my little fairy tale is done, the end, finita la commedia. They lived happily ever after.

> Note: The next 2 sentences are to be read with a Lord of the Rings voice, as Galadriel (the narrator played by Cate Blanchett) when she pronounces that the Dark Lord Sauron has lived and is biding his time… it is very important.

But it wasn’t the end. Somehow, one question was left completely unaddressed. 

The question of how our technological ecosystem affects our students? Often I hear that today’s students are “digital natives”, who have nothing to learn from the school in terms of technology. They are amazing at it! In fact, so it is said, they are better at it than most teachers!

That students are better at figuring out tech may well be true, but this obfuscates the fact that there are some technologies which can greatly enhance our students’ development. These forms of tech are not only a support to their normal learning, but are deeply transformative experiences in their own right.

There is a possibility to conceive of the technologies that we deploy at school through another lens. Are we setting up our students merely to be effective consumers of the ongoing technological revolution? Can we instead give them the formation they need to be among its creators and innovators?

I do not mean just coding; only a few of our graduates will become software developers. I am referring to something more fundamental, without which even a very promising software developer may become only an average Google or Facebook factory worker. I am describing technologies that can enhance students' self-driven mindset, mold them into natural team-players, and push them to practice personal initiative and hard innovative work skills.

Intentionally applied, these technologies have the power to prepare our students for the world of tomorrow. This is every school’s default mission. The Technology Strategic plan, its mission and goals, should reflect on what our students are getting from the school technology experiences, beyond student one-to-one devices and Instructional Technology apps.

If you are curious about what exactly I am talking about, take a look at my Projects section. Both OnwardTrek and StartupTryouts employ these types of transformative technologies. The need for these student experiences is abundantly clear to any professional who is working in today’s enterprise world.

Strangely, this is a great challenge in education. We are still a predominantly vertical system where everything, in both teaching and administration, comes down from top to bottom. And so the same holds true in Instructional Technology. For example, when your school last considered your next Learning Management System, have you looked at which one supports student independent studies and projects? I have and almost none of them do; not Schoology, not Clever and not PowerSchool. I think the reason why is because we, who work at schools, are not having these kinds of discussions. We are too busy making technology effortless and pain-free. We are only playing defense with our technology. That is not how you win, neither in sports nor elsewhere.

Perhaps, we are still under the impression that the sole role of a school’s Technology Department is to reduce that great headache that we are all so familiar with. But it is much more than that.

I encourage schools to have this conversation. We will never know unless we allow ourselves to ask questions about what technologies can do for us and what their limits can be.

And if we are open to unexpected answers.
