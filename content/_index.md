Greetings!

I am Alex, a K12 school technologist, and I am here to ask some questions from which we all can gain insights into where the education industry goes from here. If I can pick a single point, the first brick, from where to start building the future school, it is a pedagogical model that supports self-determination, a mindset and habit to be learned by students through the entirety of their schooling experience.
